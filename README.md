# Weblogic Operator for Kubernetes #

Export all the neede vars to make easy reading and understaing


```bash
export OPERATOR_NS=sample-weblogic-operator-ns
export CLONED_REPO=~/projects/oracle/weblogic-kubernetes-operator
export WEBLOGIC_USERNAME=weblogic
export WEBLOGIC_PASSWORD=Welcome1
export WEBLOGIC_DOMAIN=sample-domain1
export WEBLOGIC_DOMAIN_NS="$WEBLOGIC_DOMAIN-ns"
export WEBLOGIC_IMAGE_OUTPUT_DIRECTORY=$(pwd)/maleficarum
export JAVA_HOME=<HOME>
export WEBLOGIC_VERSION="14.1.1.0"
export WEBLOGIC_OPERATOR_VERSION="3.4.1"
```

## Clone de remote operator repo

```bash
git clone https://github.com/oracle/weblogic-kubernetes-operator.git && cd $CLONED_REPO
git pull origin "v$WEBLOGIC_OPERATOR_VERSION"
git switch -c "v$WEBLOGIC_OPERATOR_VERSION"
```

### Install the ingress proxy operator (traefik)

```bash
helm repo add traefik https://containous.github.io/traefik-helm-chart/ --force-update

helm install traefik-operator traefik/traefik \
    --namespace traefik \
    --values kubernetes/samples/charts/traefik/values.yaml \
    --set "kubernetes.namespaces={traefik}"
```

### Install the weblogic operator 

This will create a namespace to store the operator and creates a new service account to interact with the operator


```bash
kubectl create namespace $OPERATOR_NS
kubectl create serviceaccount -n $OPERATOR_NS sample-weblogic-operator-sa

cd $CLONED_REPO
helm install sample-weblogic-operator kubernetes/charts/weblogic-operator \
  --namespace $OPERATOR_NS \
  --set image=ghcr.io/oracle/weblogic-kubernetes-operator:$WEBLOGIC_OPERATOR_VERSION \
  --set serviceAccount=sample-weblogic-operator-sa \
  --set "enableClusterRoleBinding=true" \
  --set "domainNamespaceSelectionStrategy=LabelSelector" \
  --set "domainNamespaceLabelSelector=weblogic-operator\=enabled" \
  --wait 
```

Check for the operator pods status

```bash
 kubectl get pods -n $OPERATOR_NS
 kubectl logs -n $OPERATOR_NS -c weblogic-operator deployments/weblogic-operator
```

### Create the cluster config

```bash
kubectl create namespace $WEBLOGIC_DOMAIN_NS
kubectl label ns $WEBLOGIC_DOMAIN_NS weblogic-operator=enabled
```

This step will create the domain yaml definition, the docker local image and the summary file.

```bash
kubernetes/samples/scripts/create-weblogic-domain-credentials/create-weblogic-credentials.sh  -u $WEBLOGIC_USERNAME -p $WEBLOGIC_PASSWORD -n $WEBLOGIC_DOMAIN_NS -d $WEBLOGIC_DOMAIN
cd kubernetes/samples/scripts/create-weblogic-domain/domain-home-in-image
```

It is important to note tha the follow parameter would be required to be modified.

```text

The domain name to be created
domainUID: sample-domain1 

The final image created. Recommended to give a proper name to the image
image: Leave empty unless you need to tag the new image that the script builds to a different name. For example if you are using a remote cluster that will need to pull the image from a container registry, then you should set this value to the fully qualified image name. Note that you will need to push the image manually.

The secret created which contains the weblogic console credentials
weblogicCredentialsSecretName: sample-domain1-weblogic-credentials

The target namespace
namespace: sample-domain1-ns

The base image.
domainHomeImageBase: container-registry.oracle.com/middleware/weblogic:14.1.1.0

Whether to expose the console or not.
exposedAdminNodePort: true
```

```bash
cp create-domain-inputs.yaml my-inputs.yaml
./create-domain.sh -i my-inputs.yaml -o $WEBLOGIC_IMAGE_OUTPUT_DIRECTORY -u $WEBLOGIC_USERNAME -p $WEBLOGIC_PASSWORD -e
```

### Docker push for custom images

The custom images would be needed to custimize the weblogic startup process based on the Dockerfile on this repo.

```bash
docker login <region-key>.ocir.io
```

Where region key is located at https://docs.oracle.com/en-us/iaas/Content/Registry/Concepts/registryprerequisites.htm#regional-availability

When prompted for Username, enter the name of the user you will be using with Oracle Functions to create and deploy functions, in the format:

```bash
<tenancy-namespace>/<username>
```

where <tenancy-namespace> is the auto-generated Object Storage namespace string of the tenancy in which to create repositories (as shown on the Tenancy Information page). For example, ansh81vru1zp/jdoe@acme.com.

Note that for some older tenancies, the namespace string might be the same as the tenancy name in all lower-case letters (for example, acme-dev).

If your tenancy is federated with Oracle Identity Cloud Service, use the format 

```bash
<tenancy-namespace>/oracleidentitycloudservice/<username>.
```

The tenancy name space is located at the tenancy details, clicking in the Profile icon -> Tenancy: <tenancy name> under Object Storage Settings

When prompted for Password, enter the user's Oracle Cloud Infrastructure auth token; to fetch Auth Token, go to "Profile icon", and click in the "User Settings" option, and click in the "Auth Token" menu to create the token. This token is available to copy once so copy and save it in a safe place.

Once the login is performed, you can build the custom image as follows:

```bash
docker build -t <oci-region-ocir>.<tentant-namespace>/<repo-name>:<tag> .
docker push <oci-region-ocir>.<tentant-namespace>/<repo-name>:<tag>
```
Once is pushed the image, you can refer to it in your kubernetes yaml deployment files using the proper tag.

### Using custom image to deploy it to kubernetes

In the create domain process, as result of it, you will get a persisted domain.yaml which contains the full specification of the domain resources. You have to change the reference of the base image to you custom pushed image.

Also you have to create a secret to allow kubernetes to pull the custom image from the private repository:

```bash
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=<path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson
```

After secret created, you have to refer to it from you domain.yaml as follows :

```bash
imagePullSecrets:
- name: "regcred"
```

and you can apply the domain.yaml

```bash
kubectl apply -f domain.yaml
```
### Check status

```bash
kubectl describe domain $WEBLOGIC_DOMAIN -n $WEBLOGIC_DOMAIN_NS
kubectl get pods -n $WEBLOGIC_DOMAIN_NS
kubectl get services -n $WEBLOGIC_DOMAIN_NS
kubectl get services -n $WEBLOGIC_DOMAIN_NS
```

#### Optional

```bash
helm upgrade traefik-operator traefik/traefik \
    --namespace traefik \
    --reuse-values \
    --set "kubernetes.namespaces={traefik,$WEBLOGIC_DOMAIN_NS}"

helm install sample-domain1-ingress kubernetes/samples/charts/ingress-per-domain \
   --namespace $WEBLOGIC_DOMAIN_NS \
   --set wlsDomain.domainUID=s$WEBLOGIC_DOMAIN \
   --set traefik.hostname=sample-domain1.org

kubectl get all -n traefik   

curl -v -H 'host: sample-domain1.org' http://129.153.141.250/weblogic/ready
```

#### To expose admin console.

Edit the service to change to LoadBalancer

```bash
kubectl edit service sample-domain1-admin-server-ext  -n $WEBLOGIC_DOMAIN_NS
```

# Weblogic Domain home in PV

If any resource exist within the namespace, remove it : 

```bash
kubectl delete ns sample-domain2-ns
kubectl delete -f sample-domain2-weblogic-sample-pv.yaml
```

It is important to attach a shell in each worker to remove the previous (if exists) directories : 

```bash
kubectl node-shell 10.2.3.235 && k node-shell 10.2.3.210 && k node-shell 10.2.3.247
```

Once in the workers remove the previous domain info

```bash
rm -rf /scratch/* && chmod -R 777 /scratch/
```

Create the next resources in the next order, this will create a namespace with the operator annotation, creates the secret and PV/PVC to be ready to create the job : 

```bash
k create -f ~/penoles/ns.yaml && k apply -f ~/penoles/secret.yaml && k apply -f ~/penoles/pv-pvcs/sample-domain2-weblogic-sample-pv.yaml  && k apply -f ~/penoles/pv-pvcs/sample-domain2-weblogic-sample-pvc.yaml
```

create the domain exeuting 

weblogic-kubernetes-operator/kubernetes/samples/scripts/create-weblogic-domain/domain-home-on-pv/create-domain.sh -i myinputs.yaml -o ~/penoles -e

## Directory sync between workers

Would be needed to copy the domain directory from the job pod to each node to have the same Security seed on each server.
